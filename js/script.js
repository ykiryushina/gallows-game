let words = [
    'program',
    'monkey',
    'wonderful',
    'pancake',
    'potato',
    'cucumber',
    'journal',
    'straightforward',
    'flower',
    'lupin',
    'watch',
    'football',
    'hockey',
    'volleyball'
];

let word = words[Math.floor(Math.random() * words.length)];

let answerArray = [];

for (let i = 0; i < word.length; i++) {
    answerArray[i] = '_';
}

let remainingLetters = word.length;

while (remainingLetters > 0) {
    alert(answerArray.join(' '));

    let guess = prompt('Guess a letter or press "Cancel" to quit the game.');
    if (guess === null) {
        break;
    } else if (guess.length !== 1) {
        alert('Please, enter one letter only.')
    } else {
        for (let j = 0; j < word.length; j++) {
            if (word[j] === guess.toLowerCase()) {
                answerArray[j] = guess;
                remainingLetters--;
            }
        }
    }
}

if (remainingLetters == 0) {
    alert(answerArray.join(' '));
    alert('Well done! We guessed the word ' + word);
} else {
    alert('You didn\'t guess the word this time! It was the word ' + word);
}

